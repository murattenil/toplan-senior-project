const jwt = require('jsonwebtoken')
const environment = require('../environment');

const authenticate = (req, res, next) => {
    if (req.headers.authorization) {
        const token = req.headers.authorization.split(" ")[1]
        const decode = jwt.verify(token, environment.secretKey)
        req.user = decode
        next()
    } else {
        res.status(500).json({
            message: 'Authentication Failed!'
        })
    }

}

module.exports = authenticate;