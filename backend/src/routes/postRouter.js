const { Router } = require('express')
const controllers = require('../controllers')
const router = Router()
const authenticate = require('../middleware/authenticate');

router.post('/create', authenticate, controllers.postController.createPost)
router.get('/:id', authenticate, controllers.postController.findOne)
router.put('/:id', authenticate, controllers.postController.updateOne)
router.delete('/:id', authenticate, controllers.postController.deleteOne)
router.get('/', authenticate, controllers.postController.findAll);
router.get('/findByCity/:city', authenticate, controllers.postController.findPostsByCity);

module.exports = router;


/// create => Post => headers.auth => autho


