const { Router } = require('express')
const controllers = require('../controllers')
const router = Router();

router.get('/suggest', controllers.suggestionController.ActSuggetion);
router.put('/update', controllers.suggestionController.update);

module.exports = router;