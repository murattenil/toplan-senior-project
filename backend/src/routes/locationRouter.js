const { Router } = require('express')
const controllers = require("../controllers");
const router = Router();

router.post('/create', controllers.locationController.createLocation);
router.get('/findAll', controllers.locationController.findAll);
router.get('/:id', controllers.locationController.findOne);
router.put('/:id', controllers.locationController.updateOne);
router.delete('/:id', controllers.locationController.deleteOne);

module.exports = router;

