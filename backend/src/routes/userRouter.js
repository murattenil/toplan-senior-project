const { Router } = require('express')
const controllers = require('../controllers')
const router = Router()

const authenticate = require('../middleware/authenticate')

router.post('/create', authenticate, controllers.userController.createUser);
router.get('/findAll', controllers.userController.findAll);
router.get('/:id', authenticate, controllers.userController.findOne);
router.put('/:id', controllers.userController.updateOne);
router.delete('/:id', controllers.userController.deleteOne);

module.exports = router;