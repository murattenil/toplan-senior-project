const { Router } = require('express')
const controllers = require('../controllers')
const router = Router();

router.put('/:id', controllers.ratingController.userRate);

module.exports = router