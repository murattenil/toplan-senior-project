const { Router } = require('express')
const chatController = require("../controllers/chatController");
const middleware = require('../middleware/authenticate')
const router = Router();

router.post('/create/:receiver', middleware, chatController.createChatRoom)
router.get('/allrooms', middleware, chatController.getAllRooms)
router.post('/update', middleware, chatController.updateMessages)
router.get('/getroom', middleware, chatController.getRoom)

module.exports = router;