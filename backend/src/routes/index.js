const categoryRouter = require('./categoryRouter');
const postRouter = require('./postRouter');
const userRouter = require('./userRouter');
const auth = require('./auth')
const chatRouter = require('./chatRouter')
const ratingRouter = require('./ratingRouter')
const locationRouter = require('./locationRouter')
const suggestionRouter = require('./suggestionRouter')
module.exports = {
    categoryRouter,
    postRouter,
    userRouter,
    auth,
    chatRouter,
    ratingRouter,
    locationRouter,
    suggestionRouter
}