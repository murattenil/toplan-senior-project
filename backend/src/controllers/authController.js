const User = require("../models/UserSchema");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { check, validationResult } = require("express-validator");
const environment = require("../environment");

const validate = [
    check("name")
        .isLength({ min: 2 })
        .withMessage("Name must be at least 2 characters"),
    check("password")
        .isLength({ min: 6 })
        .withMessage("Passwords must be at least 6 characters"),
];

const register = async (req, res) => {
    const email = req.body.email;
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }

    const userExist = await User.findOne({ email: email });
    if (userExist) {
        res.status(400).json("User already exist");
    } else {
        bcrypt.hash(req.body.password, 10, function (err, hashedPass) {
            if (err) {
                res.json({
                    error: err,
                });
            }
            const user = new User({
                name: req.body.name,
                email: req.body.email,
                phone: req.body.phone ? req.body.phone : "null",
                password: hashedPass,
            });
            user
                .save()
                .then((user) => {
                    res.json({
                        message: "User Added Successfully!",
                    });
                })
                .catch((error) => {
                    res.json({
                        message: "An error occured!",
                    });
                });
        });
    }
};

const login = async (req, res) => {
    const { email, password } = req.body;

    console.log(email)
    const user = await User.findOne({ email: email });

    if (user) {
        const isSuccess = await bcrypt.compare(password, user.password);
        if (isSuccess) {
            const { _id, email } = user;
            const token = jwt.sign({ _id }, environment.secretKey, {
                expiresIn: "72h",
            });
            console.log("token" + token);
            res.status(201).json({ token, user: { _id, email } });
        } else {
            res.status(400).json("Password is not correct");
        }
    } else {
        res.status(400).json("User does not exist");
    }
};
module.exports = { register, login, validate };
