const categoryController = require('./categoryController');
const postController = require('./postController');
const userController = require('./userController');
const authController = require('./authController');
const chatController = require('./chatController');
const ratingController = require('./ratingController');
const locationController = require('./locationController');
const suggestionController = require('./suggestionController')

module.exports = {
    categoryController,
    postController,
    userController,
    authController,
    chatController,
    ratingController,
    locationController,
    suggestionController
}