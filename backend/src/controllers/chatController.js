const models = require('../models');

const createChatRoom = async (req, res) => {
    try {
        const receiver = await models.UserSchema.findOne({ name: req.params.receiver });
        const sender = await models.UserSchema.findById(req.user._id);
        const room = await models.ChatSchema.findOne({ name: generateRoomName(sender, receiver) }).populate('users messages');
        if (!room) {
            const newRoom = new models.ChatSchema();
            newRoom.users = [sender._id, receiver._id]
            newRoom.name = generateRoomName(sender, receiver);
            const nr = await newRoom.save().then(room => room.populate('users messages'))
            res.status(201).json(nr);
        } else {
            res.status(200).json(room)
        }
    } catch (error) {
        res.json(error);
    }

}

const generateRoomName = (sender, receiver) => {
    if (sender.name.localeCompare(receiver.name) === -1) {
        return sender.name + '-' + receiver.name;
    } else if (sender.name.localeCompare(receiver.name) === 1) {
        return receiver.name + '-' + sender.name;
    }
}


const getAllRooms = async (req, res) => {
    const rooms = await models.ChatSchema.find();
    res.send({ results: rooms })
}

const updateMessages = async (req, res) => {
    try {
        const message = req.body.message;
        const userId = req.body._id;
        console.log(userId)
        const messageContainer = { message: message, username: req.user.username, userId: userId }
        console.log(messageContainer)
        const room = await models.ChatSchema.findById(req.body.roomId);

        room.messages.push(messageContainer)
        await models.ChatSchema.updateOne(
            { _id: req.body.roomId },
            {
                $set: {
                    messages: room.messages,
                },
            }
        );
        res.send({ room })
    } catch (err) {
        console.log(error)
        res.send(err)
    }
}

const getRoom = async (req, res) => {
    try {
        const room = await models.ChatSchema.findById(req.query.roomId)
        console.log(req.query.roomId)
        res.send({ result: room })
    } catch (err) {
        res.send(err)
    }
}


module.exports = { createChatRoom, getAllRooms, updateMessages, getRoom };