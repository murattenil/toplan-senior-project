const models = require("../models");

module.exports = class LocationController {

    static async createLocation(req, res) {
        try {
            const locname = req.body.locname;
            const newLocation = new models.LocationSchema({
                name: locname
            })
            await newLocation.save()
            res.json(newLocation);
        } catch (err) {
            res.status(400).json(err)
        }

    }
    static async findOne(req, res) {
        const { id } = req.params;
        const location = await models.LocationSchema.findById(id);
        res.json(location);
    }

    static async updateOne(req, res) {
        const { id } = req.params;
        const { name } = req.body;
        await models.LocationSchema.updateOne({ _id: id }, {
            $set: {
                name: name
            }
        })
        res.json('Updated');
    }

    static async deleteOne(req, res) {
        const { id } = req.params;
        await models.LocationSchema.findByIdAndDelete(id);
        res.json('deleted');
    }

    static async findAll(req, res) {
        const locations = await models.LocationSchema.find();
        res.status(200).json(locations);
    }
}
