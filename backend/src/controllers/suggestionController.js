const { model } = require('mongoose');
const models = require('../models');

const ActSuggetion = async (req, res) => {
    const { id } = req.params;
    const { art, sport } = req.body
    const { age } = req.body.age
    const { rate } = req.body.rate
    const { pastAct } = req.body;

    // q1
    const rateOfActivity = await models.UserSchema.find(id).then(id =>
        art = id.art,
        sport = sport.art,
    )
    if (art > sport) {
        result = art / sport;
        res.json('User mostly prefer art' + result)
    } else {
        result = sport / art;
        res.json('User mostly prefer sport' + result)
    }

    // q2
    const ageOfActivity = await models.PostSchema.find(id).populate(joinedUsers)
        .then(joinedUsers => {
            ages = null,
                userCounterinActivity = null,
                joinedUsers.map(id => {
                    userCounterinActivity = userCounterinActivity + 1;
                    ages = ages + id.age;
                }),
                ageAvarage = ages / userCounterinActivity,
                res.json('Avarage Rating =' + ageAvarage)
        })


    // q3
    const genderOfActivity = await models.PostSchema.find(id).populate(joinedUsers)
        .then(joinedUsers => {
            male = null,
                female = null,
                joinedUsers.map(id => {
                    userGender = id.gender.toLowerCase();
                    if (userGender == male) {
                        male = male + 1;
                    } else if (userGender == female) {
                        female = female + 1;
                    }
                }),

                sumOfGender = male + female,
                maleRatio = male / sumOfGender,
                femaleRatio = female / sumOfGender,

                res.json('Male ratio in Activiy = ' + maleRatio,
                    'Female ratio in Activity = ' + femaleRatio
                )
        })



    // q4
    const userRatesOfActivityJoined = await models.PostSchema.find(id).populate(joinedUsers)
        .then(user => {
            ratingUsers = null;
            counter = null;
            user.map(id => {
                counter = counter + 1;
                ratingUsers = ratingUsers + id.rating;
            }),

                ratingAvarage = ratingUsers / counter,

                res.json('User rates in Activity = ' + ratingAvarage)
        })

    // k value = the Minimum Value Required To Suggest Activity
    const kValue = 0.5;

    // t1
    const userActivityRatio = await model.UserSchema.find(id)
        .then(id => {
            userSport = id.sport;
            userArt = id.art;
            totalActivity = userSport + userArt;
            rateOfSport = userSport / totalActivity;
            rateOfArt = userArt / totalActivity;
        })
    try {
        if (rateOfSport > rateOfArt) {
            res.json('Rate of Sport = ' + rateOfSport)
        } else {
            res.json('Rate of Art = ' + rateOfArt)
        }
    } catch (err) {
        res.status(400).json(err)
    }

    // t2
    const userPastActivityAgeRatio = await model.UserSchema.find(id)
        .then(id => {
            pastActivities = id.pastAct;
            pastActivities.map(act => {
                activities = [];
                oneActivityRate = null;
                tempAge = null;
                counter = null;
                if (oneActivityRate == null) {
                    act.map(item => {
                        tempAge = tempAge + item.age
                        counter = counter + 1;
                    })
                    oneActivityRate = tempAge / counter;
                    activities.push(oneActivityRate)
                    tempAge = null;
                    counter = null;
                    oneActivityRate = null;
                    return;
                }
                try {
                    total = null;
                    count = null;
                    activities.map(item => {
                        avarage = total + item
                        count = count + 1
                    })
                    avarage = total / count;
                    result = avarage
                    res.json('User past Activity Age Ratio ' + result);
                } catch (err) {
                    res.json(err)
                }

            })
        })

    // t3
    const userPastActivityGenderRatio = await model.UserSchema.find(id)
        .then(id => {
            pastActivities = id.pastAct
            pastActivities.map(gender => {
                activities = []
                oneActivityRate = null;
                tempAge = null;
                counter = null;

                if (oneActivityRate == null) {
                    gender.map(item => {
                        tempAge = tempAge + item.gender
                        counter = counter + 1;
                    })
                    oneActivityRate = tempAge / counter;
                    activities.push(oneActivityRate)
                    tempAge = null;
                    counter = null;
                    oneActivityRate = null;
                    return;
                }

                try {
                    total = null;
                    sameGender = null;
                    activities.map(item => {
                        if (item.gender == _id.gender) {
                            sameGender = sameGender + 1;
                        }
                        total = total + 1;
                    })
                    avarage = sameGender / total;
                    result = avarage
                    res.json('User past Activity Gander Ratio ' + result);
                } catch (err) {
                    res.json(err)
                }
            })
        })

    // t4
    const userPastActivityRateRatio = await model.UserSchema.find(id)
        .then(id => {
            pastActivities = id.pastAct
            pastActivities.map(rate => {
                activities = []
                oneActivityRate = null;
                tempAge = null;
                counter = null;

                if (oneActivityRate == null) {
                    gender.map(item => {
                        tempAge = tempAge + item.gender
                        counter = counter + 1;
                    })
                    oneActivityRate = tempAge / counter;
                    activities.push(oneActivityRate)
                    tempAge = null;
                    counter = null;
                    oneActivityRate = null;
                    return;
                }

                try {
                    total = null;
                    count = null;
                    activities.map(item => {
                        avarage = total + item
                        count = count + 1
                    })
                    avarage = total / count;
                    result = avarage
                    res.json('User past Activity Rate Ratio ' + result);
                } catch (err) {
                    res.json(err)
                }
            })
        })


    // h Activity Informations
    const activityInformation = Math.max(0,
        rateOfActivity * userActivityRatio +
        ageOfActivity * userPastActivityAgeRatio +
        genderOfActivity * userPastActivityGenderRatio +
        userRatesOfActivityJoined * userPastActivityRateRatio -
        kValue
    )

    return activityInformation;

}

// Σt4𝑖=1i = ti*n/( n+1 )+[ mi / (n+1) ]
const update = async (req, res) => {
    const { id } = req.params;
    const { rate } = req.body;
    const { pastAct } = req.body;
    const SigmaPastAct = userPastActivityAgeRatio * pastAct / (pastAct + 1) + [1 / (pastAct + 1)];
    const SigmaRating = userPastActivityRateRatio * rate / (pastAct + 1) + [rate / (pastAct + 1)];
    await models.UserSchema.updateOne(id), {
        $set: {
            pastAct: SigmaPastAct,
            rating: SigmaRating,
        }
    }
    res.json('User Past Activity updated ' + pastAct,
        'User Rating updated ' + rating);

}

module.exports = { ActSuggetion, update };

