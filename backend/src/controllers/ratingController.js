const models = require("../models");

module.exports = class ratingController {
    static async userRate(req, res) {
        try {
            const { num } = req.params;
            const { rateAvg, rateCount } = req.body
            const user = await models.UserSchema.find({ user: req.params })
            await models.UserSchema.updateOne({ _id: user }, {
                $set: {
                    rateAvg: rateAvg * rateCount,
                    rateAvg: rateAvg + num,
                    rateCount: rateCount + 1,
                    rateAvg: rateAvg / rateCount,
                }
            })
            res.json('new ratingAvg' + rateAvg)
        }
        catch (err) {
            res.status(400).json(err)
        }


    }

    static async sortRates(req, res) {
        const { avg } = req.params
        const sorting = await models.UserSchema.find().sort({ rateAvg: avg })
        res.json(sorting)

    }

}
