const models = require("../models");

module.exports = class CategoryController {

    static async createCategory(req, res) {
        try {
            const catname = req.body.catname;
            const newCategory = new models.CategorySchema({
                name: catname
            })
            await newCategory.save()
            res.json(newCategory);
        } catch (err) {
            res.status(400).json(err)
        }

    }
    static async findOne(req, res) {
        const { id } = req.params;
        const category = await models.CategorySchema.findById(id);
        res.json(category);
    }

    static async updateOne(req, res) {
        const { id } = req.params;
        const { name } = req.body;
        await models.CategorySchema.updateOne({ _id: id }, {
            $set: {
                name: name
            }
        })
        res.json('Updated');
    }

    static async deleteOne(req, res) {
        const { id } = req.params;
        await models.CategorySchema.findByIdAndDelete(id);
        res.json('deleted');
    }
    static async findAll(req, res) {
        const categories = await models.CategorySchema.find();
        res.status(200).json(categories);
    }
}
