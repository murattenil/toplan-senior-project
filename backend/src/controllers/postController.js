const models = require("../models");
module.exports = class PostController {

    static async createPost(req, res) {
        try {
            const { title, category, description, time, location } = req.body;
            const newPost = new models.PostSchema({
                title: title,
                category: category,
                description: description,
                time: time,
                location: location,
                userId: req.user._id,

            })
            await newPost.save()
            res.status(201).json(newPost);
        } catch (err) {
            res.status(400).json(err)
        }

    }
    static async findOne(req, res) {
        const { id } = req.params;
        const post = await models.PostSchema.findById(id).populate('category userId');
        res.json(post);
    }

    static async updateOne(req, res) {
        const { id } = req.params;
        const { title, category, description, time, location } = req.body;
        await models.PostSchema.updateOne({ _id: id }, {
            $set: {
                title: title,
                category: category,
                description: description,
                time: time,
                location: location
            }
        })
        res.json('Updated');
    }

    static async deleteOne(req, res) {
        const { id } = req.params;
        await models.PostSchema.findByIdAndDelete(id);
        res.json('deleted');
    }
    static async findAll(req, res) {
        const posts = await models.PostSchema.find().populate('category userId');
        res.json(posts);
    }

    static async findPostsByCity(req, res) {
        const { city } = req.params;

        const getAllByCities = await models.PostSchema.find({ location: city }).populate('category userId')
        res.json(getAllByCities)
    }

}