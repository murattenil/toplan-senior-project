const models = require("../models");

module.exports = class UserController {
    static async createUser(req, res) {
        try {
            const catname = req.body.catname;
            const newUser = new models.CategorySchema({
                name: catname
            })
            await newUser.save()
            res.json(newUser);
        } catch (err) {
            res.status(400).json(err)
        }

    }
    static async findOne(req, res) {
        const { id } = req.params;
        const user = await models.UserSchema.findById(req.user._id);
        res.json(user);
    }

    static async updateOne(req, res) {
        const { id } = req.params;
        const { name } = req.body;
        await models.UserSchema.updateOne({ _id: id }, {
            $set: {
                name: name
            }
        })
        res.json('Updated');
    }

    static async deleteOne(req, res) {
        const { id } = req.params;
        await models.UserSchema.findByIdAndDelete(id);
        res.json('deleted');
    }
    static async findAll(req, res) {
        const users = await models.UserSchema.find();
        res.json(users);
    }
}