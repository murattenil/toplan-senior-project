const express = require('express')
const mongoose = require('mongoose');
const routes = require('./routes')
const cors = require('cors');
const bodyParser = require('body-parser');
const app = express();
const server = require('http').createServer(app);
const io = require("socket.io")(server);
const models = require("./models");
const jwt = require('jsonwebtoken')
const environment = require('./environment');
const PORT = 5000;
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(cors());

app.use('/auth', routes.auth);
app.use('/user', routes.userRouter);
app.use('/post', routes.postRouter);
app.use('/category', routes.categoryRouter);
app.use('/chat', routes.chatRouter);
app.use('/rating', routes.ratingRouter);
app.use('/location', routes.locationRouter);
app.use('/suggestion', routes.suggestionRouter)

mongoose.connect('mongodb+srv://dbMurat:23734658070@cluster0.nc3sy.mongodb.net/myFirstDatabase?retryWrites=true&w=majority',
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
  }
)
  .then(() => {
    console.log('server started');
    //    app.listen(5000);
  })
  .catch(err => console.log(err));


/* io.use(async (socket, next) => {
  try {
    const token = socket.handshake.query.token;
    const payload = await jwt.verify(token, environment.secretKey);
    socket.userId = payload._id;
    next();
  } catch (error) {

  }
}) */

io.on('connection', (socket) => {
  console.log("socket connected" + socket.id)

  socket.on('disconnect', (reason) => {
    console.log('User disconnected ' + reason)
  })

  socket.on('sendmessage', async ({ text, roomId }) => {
    console.log('message sent' + text.message + roomId)
    //const receiverId = await models.UserSchema.findOne({ _id: userId });
    const createdMessage = await models.MessageSchema.create({
      text: text.message,
      roomId: roomId,

    })
    io.to(roomId).emit('newmessage', { text: createdMessage.text, roomId: createdMessage.roomId })
    await createdMessage.save()
    const room = await models.ChatSchema.findById(roomId);
    await room.updateOne({
      $push: {
        messages: createdMessage
      }
    });

  })

  socket.on('joinroom', roomId => {
    socket.join(roomId)
    console.log('User joined ' + socket.id)
  })

  socket.on('leaveroom', roomId => {
    socket.leave(roomId)
    console.log('User leave ' + data._id)
  })

})


server.listen(PORT, () => console.log(`Server now running on port ${PORT}!`));
