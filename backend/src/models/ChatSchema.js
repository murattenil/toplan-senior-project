const mongoose = require('mongoose')

const RoomSchema = new mongoose.Schema({
    name: { type: String },
    messages: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Message'
    }],
    users: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        }
    ],
    /* time: { type: Date, default: new Date().getTime } */

})

module.exports = mongoose.model('Room', RoomSchema);

