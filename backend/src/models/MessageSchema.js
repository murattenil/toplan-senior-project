const mongoose = require('mongoose')
const MessageSchema = new mongoose.Schema({
    text: { type: String },
    roomId: { type: String, required: true },
    //userId: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },

    /* time: { type: String, default: Date.now } */
})
module.exports = mongoose.model('Message', MessageSchema);