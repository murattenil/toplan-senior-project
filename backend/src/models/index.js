const CategorySchema = require("./CategorySchema");
const PostSchema = require("./PostSchema");
const UserSchema = require("./UserSchema");
const ChatSchema = require("./ChatSchema");
const MessageSchema = require("./MessageSchema");
const LocationSchema = require("./LocationSchema");

module.exports = {
    CategorySchema,
    PostSchema,
    UserSchema,
    ChatSchema,
    MessageSchema,
    LocationSchema
};


/* (async() => {
    const newModel = new CategorySchema({
        name:'category1'
    })
    await newModel.save();
    console.log('saved')
})() */