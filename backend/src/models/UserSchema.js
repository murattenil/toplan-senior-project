const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
    name: { type: String, required: true },
    email: { type: String, unique: true, required: true },
    password: { type: String, required: true },
    photo: String,
    phone: String,
    age: String,
    gender: String,
    contacts: [{
        username: { type: String, required: true },
        userId: { type: String, required: true },
        createdAt: { type: String, default: Date.now },
    }],
    rating: [{
        rateAvg: { type: String },
        rateCount: { type: String }
    }],
    pastAct: [{
        art: { type: Number },
        sport: { type: Number }
    }],
    createdAt: { type: String, default: new Date() }
})


module.exports = mongoose.model('User', UserSchema);

