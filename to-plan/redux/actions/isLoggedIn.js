export const LOGIN_USER_SUCCESS = "LOGIN_USER_SUCCESS";
export const LOGIN_USER_FAIL = "LOGIN_USER_FAIL";
import axios from '../../src/helpers/axios'

import AsyncStorage from "@react-native-community/async-storage";
export const IsLoggedIn = () => {
    return async (dispatch) => {
        try {
            const token = await AsyncStorage.getItem('token')
            if (token) {
                const user = JSON.parse(await AsyncStorage.getItem('user'))

                dispatch({
                    type: LOGIN_USER_SUCCESS,
                    payload: { token, user }
                })
            }
        } catch (error) {
            dispatch({
                type: LOGIN_USER_FAIL,
                payload: error
            })
        }
    }
}