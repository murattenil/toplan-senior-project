export const FETCH_USER = 'FETCH_USER'
import axios from "../../src/helpers/axios"
import Constants from "../../constants/constant";
import AsyncStorage from "@react-native-community/async-storage";

export const fetchuser = () => {
    return async (dispatch) => {
        const res = await axios.get(`${Constants.URL}/user/:id`, { 'headers': { 'Authorization': AsyncStorage.getItem('token') } });

        dispatch({
            type: FETCH_USER,
            payload: res.data
        })
    }
}