export const GET_LOCATION_PENDING = 'GET_LOCATION_PENDING';
export const GET_LOCATION_DONE = 'GET_LOCATION_DONE';
export const GET_LOCATION_ERROR = 'GET_LOCATION_ERROR';
import axios from '../../src/helpers/axios';
import Constants from "../../constants/constant";

export const getLocations = () => {
    return async (dispatch) => {
        const res = await axios.get(`${Constants.URL}/location/findAll`);
        dispatch({
            type: GET_LOCATION_PENDING
        })
        if (res.status === 200) {
            dispatch({
                type: GET_LOCATION_DONE,
                payload: res.data
            })
        } else {
            dispatch({
                type: GET_LOCATION_ERROR,
                payload: res.data
            })
        }
    }
}