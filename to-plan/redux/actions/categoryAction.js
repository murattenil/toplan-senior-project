export const GET_CATEGORIES_PENDING = 'GET_CATEGORIES_PENDING';
export const GET_CATEGORIES_DONE = 'GET_CATEGORIES_DONE';
export const GET_CATEGORIES_ERROR = 'GET_CATEGORIES_ERROR'
import axios from "../../src/helpers/axios"
import Constants from "../../constants/constant";

export const getCategories = () => {

    return async (dispatch) => {
        const res = await axios.get(`${Constants.URL}/category/findAll`);
        dispatch({
            type: GET_CATEGORIES_PENDING
        })
        if (res.status === 200) {
            dispatch({
                type: GET_CATEGORIES_DONE,
                payload: res.data
            })
        } else {
            dispatch({
                type: GET_CATEGORIES_ERROR,
                payload: res.data,
            });
        }

    }
}
