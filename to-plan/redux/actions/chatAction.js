export const FETCH_CHAT = 'FETCH_CHAT';
export const CREATE_CHATROOM_DONE = "CREATE_CHATROOM_DONE";
export const CREATE_CHATROOM_FAIL = "CREATE_CHATROOM_FAIL";
export const CREATE_CHATROOM_PENDING = "CREATE_CHATROOM_PENDING";
export const EDIT_MESSAGES = "EDIT_MESSAGES"
export const ALL_ROOMS = "ALL_ROOMS"
import axios from "../../src/helpers/axios"
import Constants from "../../constants/constant";


import AsyncStorage from '@react-native-community/async-storage';


export const fetchchats = () => {

    return async (dispatch) => {
        const res = await axios.get(`${Constants.URL}/chat`);

        dispatch({
            type: FETCH_CHAT,
            payload: res.data,
        });
    }
}

export const createroom = (receiver, navigation) => {
    return async (dispatch) => {
        try {
            const token = await AsyncStorage.getItem('token');
            const res = await axios(`${Constants.URL}/chat/create/${receiver}`, {
                method: 'POST',
            })
            dispatch({
                type: CREATE_CHATROOM_PENDING,
            })
            if (res.status === 200) {
                dispatch({
                    type: CREATE_CHATROOM_DONE,
                    payload: res.data
                })

            } else {
                dispatch({
                    type: CREATE_CHATROOM_FAIL,
                    payload: res.data
                })
                navigation.navigate('FindAct')
            }
        } catch (err) {

        }
    }

}

export const afterMessage = (data) => {
    return {
        type: EDIT_MESSAGES,
        payload: data
    }
}

export const getAllRooms = () => {
    return async (dispatch) => {
        try {
            const token = await AsyncStorage.getItem('token');
            const res = await axios.get(`${Constants.URL}/chat/allrooms`);
            dispatch({
                type: 'ALL_ROOMS_PENDING'
            })
            dispatch({
                type: ALL_ROOMS,
                payload: res.data.results
            })
            console.log(res.data.results)
        } catch (err) {

        }
    }

}
