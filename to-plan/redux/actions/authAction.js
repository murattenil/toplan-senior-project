export const REGISTER_USER_PENDING = "REGISTER_USER_PENDING";
export const REGISTER_USER_SUCCESS = "REGISTER_USER_SUCCESS";
export const REGISTER_USER_FAIL = "REGISTER_USER_FAIL";
export const LOGIN_USER_SUCCESS = "LOGIN_USER_SUCCESS";
export const LOGIN_USER_FAIL = "LOGIN_USER_FAIL";
export const LOGIN_USER_PENDING = "LOGIN_USER_PENDING";

import AsyncStorage from "@react-native-community/async-storage";
import axios from "../../src/helpers/axios"
import Constants from "../../constants/constant";

export const registerUser = (name, email, password, navigation) => {

    return async (dispatch) => {
        try {
            dispatch({
                type: REGISTER_USER_PENDING,
            });
            //http://Ip:5000/auth/register
            fetch(`${Constants.URL}/auth/register`, {
                method: "POST",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    name: name,
                    email: email,
                    password: password,

                }),
            })
                .then((res) => {
                    return res.json();
                })
                .then((data) => {
                    console.log(data)
                    dispatch({
                        type: REGISTER_USER_SUCCESS,
                        payload: data,
                    });
                    navigation.replace("Login");
                });
        } catch (error) {
            dispatch({
                type: REGISTER_USER_FAIL,
                payload: error,
            });
        }
    };
};

export const loginUser = (email, password, navigation) => {
    return async (dispatch) => {
        const res = await axios({
            url: `${Constants.URL}/auth/login`,
            method: 'POST',
            data: {
                email: email,
                password: password
            }
        })
        dispatch({
            type: LOGIN_USER_PENDING,
        })
        if (res.status === 201) {
            const { user, token } = res.data;
            await AsyncStorage.setItem("token", token);
            await AsyncStorage.setItem("user", JSON.stringify(user));
            dispatch({
                type: LOGIN_USER_SUCCESS,
                payload: { token, user }
            })
            navigation.replace("Menu");
        } else {
            dispatch({
                type: LOGIN_USER_FAIL,
                payload: res.data,
            })
        }

    };
};
