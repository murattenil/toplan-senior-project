export const FETCH_POST = 'FETCH_POST';
export const CREATE_POST_DONE = "CREATE_POST_DONE";
export const CREATE_POST_PENDING = "CREATE_POST_PENDING";
export const CREATE_POST_ERROR = "CREATE_POST_ERROR";
export const DELETE_POST = "DELETE_POST";
import axios from "../../src/helpers/axios"
/* import axios from "axios"; */
import AsyncStorage from '@react-native-community/async-storage';
import Constants from "../../constants/constant";


export const fetchposts = () => {

    return async (dispatch) => {
        const res = await axios.get(`${Constants.URL}/post`);

        dispatch({
            type: FETCH_POST,
            payload: res.data,
        });
    }
}

export const createpost = (data = {}, navigation) => {

    return async (dispatch) => {
        const token = await AsyncStorage.getItem('token');
        const res = await axios(`${Constants.URL}/post/create`, {
            method: 'POST',
            data: { ...data }
        })
        dispatch({
            type: CREATE_POST_PENDING,
        })
        if (res.status === 201) {
            dispatch({
                type: CREATE_POST_DONE,
                payload: res.data
            })
            navigation.replace('Menu')
        } else {
            dispatch({
                type: CREATE_POST_ERROR,
                payload: res.data
            })
            navigation.navigate('CreateAct')
        }
    }
}