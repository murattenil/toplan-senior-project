import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'

import postReducer from './reducers/postReducer'
import authReducer from './reducers/authReducer';
import registerReducer from './reducers/registerReducer'
import createPostReducer from './reducers/createPostReducer';
import categoryReducer from "./reducers/categoryReducer";
import { getChatReducer } from './reducers/chatReducer';
import { allRoomReducer } from "./reducers/roomReducer";
import { getUserReducer } from './reducers/userReducer';
import locationReducer from './reducers/locationReducer';


const rootReducer = combineReducers({
    posts: postReducer,
    auth: authReducer,
    register: registerReducer,
    createPost: createPostReducer,
    categoryReducer: categoryReducer,
    chat: getChatReducer,
    allRooms: allRoomReducer,
    users: getUserReducer,
    locationReducer: locationReducer
});

const middleware = composeWithDevTools(applyMiddleware(thunk));

export const store = createStore(rootReducer, middleware);
