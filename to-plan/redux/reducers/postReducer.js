import {
    FETCH_POST,

} from '../actions/postAction';

const initialState = {
    activities: [],
    pending: false,
    error: {},
}

export default function (state = initialState, action) {

    switch (action.type) {
        case FETCH_POST:
            return {
                ...state,
                activities: action.payload,
            }
        default:
            return state;
    }
}