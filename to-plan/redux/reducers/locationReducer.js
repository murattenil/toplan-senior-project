import {
    GET_LOCATION_DONE,
    GET_LOCATION_PENDING,
    GET_LOCATIONS_ERROR
} from '../actions/locationAction';

const initialState = {
    fetching: false,
    fetched: false,
    locations: [],
    error: {}
}

export default function (state = initialState, action) {
    switch (action.type) {
        case GET_LOCATION_PENDING:
            return {
                ...state,
                fetching: true
            }
        case GET_LOCATION_DONE:
            return {
                ...state,
                fetching: false,
                fetched: true,
                locations: action.payload
            }
        case GET_LOCATIONS_ERROR:
            return {
                ...state,
                error: action.payload
            }
        default:
            return state;
    }
}