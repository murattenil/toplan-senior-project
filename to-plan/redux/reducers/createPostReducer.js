import {
    CREATE_POST_DONE,
    CREATE_POST_PENDING,
    CREATE_POST_ERROR,
} from '../actions/postAction';

const initialState = {
    fetching: false,
    fetched: false,
    error: {}
}

export default function (state = initialState, action) {
    switch (action.type) {
        case CREATE_POST_PENDING:
            return {
                ...state,
                fetching: true,
            }
        case CREATE_POST_DONE:
            return {
                ...state,
                fetching: false,
                fetched: true
            }
        case CREATE_POST_ERROR:
            return {
                ...state,
                fetched: false,
                error: action.payload
            }
        default:
            return state;
    }
}
