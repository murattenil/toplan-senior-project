import {
    CREATE_CHATROOM_DONE,
    CREATE_CHATROOM_FAIL,
    CREATE_CHATROOM_PENDING,
    EDIT_MESSAGES,
    ALL_ROOMS
} from '../actions/chatAction';

const initialState = {
    fetching: false,
    fetched: false,
    room: {},
    messages: [],
    error: {}
}

export const getChatReducer = (state = initialState, action) => {
    switch (action.type) {
        case CREATE_CHATROOM_PENDING:
            return {
                ...state,
                fetching: true
            }
        case CREATE_CHATROOM_DONE:
            return {
                ...state,
                fetching: false,
                fetched: true,
                room: action.payload,
                messages: action.payload.messages
            }
        case CREATE_CHATROOM_FAIL:
            return {
                ...state,
                error: action.payload
            }
        case EDIT_MESSAGES:
            return {
                ...state,
                messages: state.messages.concat(action.payload)
            }
        default:
            return state;
    }
}