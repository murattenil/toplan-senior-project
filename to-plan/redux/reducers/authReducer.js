import {
    LOGIN_USER_SUCCESS,
    LOGIN_USER_PENDING,
    LOGIN_USER_FAIL
} from '../actions/authAction';

import AsyncStorage from '@react-native-community/async-storage';
const initialState = {
    user: {},
    done: false,
    pending: false,
    error: {},
    token: null,
}

export default function (state = initialState, action) {

    switch (action.type) {
        case LOGIN_USER_PENDING:
            return {
                ...state,
                pending: true

            }
        case LOGIN_USER_SUCCESS:
            return {
                ...state,
                user: action.payload.user,
                token: action.payload.token,
                done: true, //auth.done === true <navigate>  
                pending: false
            }

        case LOGIN_USER_FAIL:
            return {
                ...state,
                done: false,
                error: action.payload
            }
        default:
            return state;
    }
}