import {
    GET_CATEGORIES_DONE,
    GET_CATEGORIES_PENDING,
    GET_CATEGORIES_ERROR
} from '../actions/categoryAction';

const initialState = {
    fetching: false,
    fetched: false,
    categories: [],
    error: {}
}

export default function (state = initialState, action) {
    switch (action.type) {
        case GET_CATEGORIES_PENDING:
            return {
                ...state,
                fetching: true
            }
        case GET_CATEGORIES_DONE:
            return {
                ...state,
                fetching: false,
                fetched: true,
                categories: action.payload
            }
        case GET_CATEGORIES_ERROR:
            return {
                ...state,
                error: action.payload
            }
        default:
            return state;
    }
}
