const initialState = {
    fetching: false,
    fetched: false,
    rooms: []
}
export const allRoomReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'ALL_ROOMS_PENDING':
            return {
                ...state,
                fetching: true
            }
        case 'ALL_ROOMS':
            return {
                ...state,
                fetched: true,
                fetching: false,
                rooms: action.payload
            }
        default:
            return state;
    }
}