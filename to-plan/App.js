import { StatusBar } from 'expo-status-bar';
import React, { Component, useEffect } from 'react';
import { store } from './redux/store'
import { Provider, useSelector, useDispatch } from 'react-redux'
import { Login } from './src/screens/LoginScreen/Login'
import Onboarding from './src/screens/OnboardScreen/OnboardingScreen'
import Router from './src/components/Router'
import Createact from './src/screens/CreateScreen/CreateAct'
import FindAct from './src/screens/FindScreen/FindAct'
import Join from './src/screens/JoinScreen/Join'
import Cards from './src/components/Cards/Cards'
import { isLoggedIn } from './redux/actions/isLoggedIn'


export default function App() {

  return (
    <Provider store={store}>
      <StatusBar style='auto' />
      <Login />
    </Provider>
  );
}

