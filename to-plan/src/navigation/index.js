import Home from '../screens/HomeScreen/Home';
import Find from '../screens/FindScreen/FindAct';
import Create from '../screens/CreateScreen/CreateAct';
import Join from '../screens/JoinScreen/Join';
import Friends from '../screens/FriendsScreen/Friends';
import Profile from '../screens/ProfileScreen/Profile';
import Menu from '../components/Router';
import Settings from '../screens/SettingsScreen';
import Onboarding from '../screens/OnboardScreen/OnboardingScreen';
import { LoginScreen, SignupScreen } from '../screens/LoginScreen/Login'
import Chatinside from '../components/ChatInside/Chatinside';
const optionHeandler = () => ({
    headerShown: false,
});
export const navigations = [
    {
        name: 'Home',
        component: { Home },
        options={ optionHeandler }
    },
    {
        name: 'Find',
        component: { Find },
        options={ optionHeandler }
    },
    {
        name: 'Menu',
        component: { Menu },
        options={ optionHeandler }
    },
    {
        name: 'Create',
        component: { Create },
        options={ optionHeandler }
    },
    {
        name: 'Join',
        component: { Join },
        options={ optionHeandler }
    },
    {
        name: 'Friends',
        component: { Friends },
        options={ optionHeandler }
    },
    {
        name: 'Join',
        component: { Join },
        options={ optionHeandler }
    },
]