import React, { Component } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Home from '../screens/HomeScreen/Home';
import Find from '../screens/FindScreen/FindAct';
import Create from '../screens/CreateScreen/CreateAct';
import Join from '../screens/JoinScreen/Join';
import Friends from '../screens/FriendsScreen/Friends';
import Profile from '../screens/ProfileScreen/Profile';
import Menu from '../components/Router';
import { io } from 'socket.io-client';
import Settings from '../screens/SettingsScreen';
import Onboarding from '../screens/OnboardScreen/OnboardingScreen';
import { LoginScreen, SignupScreen } from '../screens/LoginScreen/Login'
import Chatinside from '../components/ChatInside/Chatinside';
import AsyncStorage from '@react-native-community/async-storage';
import Suggest from '../screens/SuggestScreen/Suggest';
import Constants from '../../constants/constant';

const AppNavigator = createStackNavigator();

const optionHeandler = () => ({
  headerShown: false,
});

export const Navigation = (props) => {

  const tk = AsyncStorage.getItem('token')
  const user = JSON.parse(AsyncStorage.getItem('user'))

  const socket = io(`${Constants.URL}`, {
    query: {
      token: tk ? tk : "",
      userId: tk ? user._id : ""
    }
  })
  socket.connect();

  return (
    <NavigationContainer>
      <AppNavigator.Navigator initialRouteName="Menu">
        <AppNavigator.Screen
          screenProps={{ socket: socket }}
          name="Home"
          component={Home}
          options={optionHeandler}
        />
        <AppNavigator.Screen
          screenProps={{ socket: socket }}
          name="Find"
          component={Find}
          options={optionHeandler}
        />
        <AppNavigator.Screen
          screenProps={{ socket: socket }}
          name="Menu"
          component={Menu}
          options={optionHeandler}
        />
        <AppNavigator.Screen
          screenProps={{ socket: socket }}
          name="Create"
          component={Create}
          options={optionHeandler}
        />
        <AppNavigator.Screen
          screenProps={{ socket: socket }}
          name="Join"
          component={Join}
          options={optionHeandler}
        />
        <AppNavigator.Screen
          screenProps={{ socket: socket }}
          name="Friends"
          component={Friends}
          options={optionHeandler}
        />
        <AppNavigator.Screen
          screenProps={{ socket: socket }}
          name="Chatinside"
          component={Chatinside}
          options={optionHeandler}
        />
        <AppNavigator.Screen
          screenProps={{ socket: socket }}
          name="Profile"
          component={Profile}
          options={optionHeandler}
        />
        <AppNavigator.Screen
          screenProps={{ socket: socket }}
          name="Onboarding"
          component={Onboarding}
          options={optionHeandler}
        />
        <AppNavigator.Screen
          screenProps={{ socket: socket }}
          name="LoginScreen"
          component={LoginScreen}
          options={optionHeandler}
        />
        <AppNavigator.Screen
          screenProps={{ socket: socket }}
          name="SignupScreen"
          component={SignupScreen}
          options={optionHeandler}
        />
        <AppNavigator.Screen
          screenProps={{ socket: socket }}
          name="Settings"
          component={Settings}
          options={optionHeandler}
        />
        <AppNavigator.Screen
          screenProps={{ socket: socket }}
          name="Suggest"
          component={Suggest}
          options={optionHeandler}
        />
      </AppNavigator.Navigator>
    </NavigationContainer>
  );
}

