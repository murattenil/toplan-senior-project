import { StyleSheet, Dimensions } from 'react-native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export const styles = StyleSheet.create({
    card: {
        shadowColor: 'black',
        shadowOpacity: 0.25,
        shadowOffset: { width: 0, height: 2 },
        borderRadius: 10,
        shadowRadius: 8,
        backgroundColor: '#fff',
        elevation: 5,
        height: windowHeight / 6,
        width: windowWidth / 1.2,
        margin: '3%',
        flexDirection: 'column'
    },
    titleContainer: {
        padding: '2%',
        flexDirection: 'row',
        alignItems: 'center'
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'gray'
    },
    subtitle: {
        fontSize: 16,
        marginStart: '2%',
        color: 'gray'
    },
    descContainer: {
        marginTop: '6%',
        flexDirection: 'row',
        justifyContent: 'space-between',

    },
    timeContainer: {
        flexDirection: 'row',
        marginLeft: '3%'
    },
    text: {
        fontSize: 14,
        color: 'grey'
    },
    userContainer: {
        flexDirection: 'row',
        paddingHorizontal: '3%'
    }


})
