import React, { Component, useState, useEffect } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { styles } from './styles';
import { useDispatch, useSelector } from 'react-redux';
import { getCategories } from '../../../redux/actions/categoryAction'
import { getLocations } from '../../../redux/actions/locationAction';
import { fetchposts } from '../../../redux/actions/postAction';


const Card = props => {
    const dispatch = useDispatch();
    const categoryReducer = useSelector(state => state.categoryReducer);
    const locationReducer = useSelector(state => state.locationReducer);
    useEffect(() => {
        dispatch(getCategories())
    }, [])

    useEffect(() => {
        dispatch(getLocations())
    }, [])

    useEffect(() => {
        dispatch(fetchposts());
    }, [dispatch]);


    return (
        <TouchableOpacity
            onPress={() => props.navigation.navigate("Join", {
                postId: props.id
            })}
        >
            <View style={styles.card}>
                <View style={styles.titleContainer}>
                    <Text style={styles.title}>
                        {props.title.length > 30 ? props.title.slice(0, 30) + '...' : props.title}
                    </Text>
                    <Text style={styles.title}>
                        - {props.category.name}

                    </Text>


                </View>

                <View style={styles.titleContainer}>
                    <Text style={styles.subtitle}>
                        - {props.description.length > 40 ? props.description.slice(0, 40) + '...' : props.description}
                    </Text>
                </View>

                <View style={styles.descContainer}>
                    <View style={styles.timeContainer}>
                        <Text style={styles.text}>
                            {props.time}
                        </Text>
                        <Text style={styles.text}>
                            - in {locationReducer.locations.map(item => {
                                if (item._id == props.location) {
                                    return (item.name)
                                }
                            })}
                        </Text>


                    </View>

                    <View style={styles.userContainer}>
                        <Text style={styles.text}>
                            {props.name}
                        </Text>
                        <Text style={styles.text}>
                            age
                        </Text>
                    </View>
                </View>
            </View>
        </TouchableOpacity>
    );
}


export default Card;