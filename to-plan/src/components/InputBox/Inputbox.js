import React, { Component, useEffect, useState } from 'react';
import {
    View,
    Text,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    Platform,
} from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { Ionicons } from '@expo/vector-icons';
import styles from './styles'

const Inputbox = (props) => {
    const dispatch = useDispatch();
    const [message, setMessage] = useState(null);
    const room = useSelector((state) => state.room)

    const sendMessage = () => {
        if (props.socket) {

            props.socket.emit("sendmessage", { text: message, roomId: props.roomId });
            setMessage("");
        }
    };


    return (
        <KeyboardAvoidingView
            behavior={Platform.OS == "ios" ? "padding" : "height"}
            keyboardVerticalOffset={100}
            style={{ width: '100%' }}
        >
            <View style={styles.container}>
                <View style={styles.mainContainer}>
                    <TextInput
                        placeholder={"Type a message"}
                        style={styles.textInput}
                        multiline
                        value={message}
                        onChangeText={(message) => setMessage({ message })}
                    />

                </View>
                <TouchableOpacity onPress={() => sendMessage()} >
                    <View style={styles.buttonContainer}>
                        <Ionicons name="send" size={24} color="white" />
                    </View>
                </TouchableOpacity>
            </View>

        </KeyboardAvoidingView>

    );

}

export default Inputbox;
