import React, { useEffect, useState } from 'react';
import { Ionicons } from '@expo/vector-icons';
import {
    View,
    Text,
    TouchableOpacity,
    FlatList,
} from 'react-native';
import InputBox from '../InputBox/Inputbox';
import { useDispatch, useSelector } from 'react-redux';

import AsyncStorage from "@react-native-community/async-storage"
import styles from './styles';
import { afterMessage, createroom } from '../../../redux/actions/chatAction';

const Chatinside = props => {
    const { roomId, socket } = props.route.params;
    const receiver = 'okan';
    const chat = useSelector(state => state.chat)
    const [messages, setMessages] = useState([
        { text: "text", roomId: "roomId" },
        { text: "text", roomId: "roomId" },
        { text: "text", roomId: "roomId" },
    ]);
    const tk = AsyncStorage.getItem('token')



    console.log('Socket CHATINSIDE::', socket.id);

    const dispatch = useDispatch()
    console.log(props)
    useEffect(() => {
        dispatch(createroom(receiver, props.navigation));
        InitializeSocket()
        /*  props.socket.on("newMessage", (outputMessage) => {
             dispatch(afterMessage(outputMessage));
         }) */
    }, [messages])


    const MessageSend = ({ message }) => (
        <View style={styles.messageSend}>
            <Text>{message}</Text>
        </View>
    );

    const MessageReceived = ({ message }) => (
        <View style={styles.messageReceived}>
            <Text>{message}</Text>
        </View>
    );
    const InitializeSocket = () => {

        socket.on("newmessage", ({ text, roomId }) => {

            console.log("Yeni mesaj geldi = " + text);
            //this.props.ChatRoomStore.AddMessage(message,username,userid)
            //this.props.ChatRoomStore.getRooms();
            setMessages([...messages,
            { text, roomId },

            ])

        });
    }
    const onPressBack = () => {
        socket.on('leaveroom', data => {
            socket.leave(data._id)
            console.log('user leave : ' + data._id)

        });
        props.navigation.goBack()
    }


    return (
        <View>
            <TouchableOpacity style={styles.backArrow}
                onPress={() => onPressBack()}
            >
                <Ionicons name="ios-arrow-back" size={30} color="black" />
            </TouchableOpacity>
            <Text style={styles.title}>{roomId}</Text>
            <FlatList
                nestedScrollEnabled
                data={messages}
                keyExtractor={item => item._id}
                renderItem={({ item }) => {

                    if (item.userId === item.userId) {
                        return (
                            <MessageReceived message={item.text} />
                        )
                    } else {
                        return (
                            <MessageSend message={item.text} />
                        )
                    }

                }}
            />
            <InputBox style={styles.inputBox} socket={socket} roomId={roomId} />
        </View>
    );
}


export default Chatinside;