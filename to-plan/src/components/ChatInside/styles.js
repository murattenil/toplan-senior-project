import { StyleSheet, Dimensions } from 'react-native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: '#EAECDD',
        alignItems: 'center'
    },
    backArrow: {
        position: 'absolute',
        top: '5%',
        left: '3%'
    },
    title: {
        marginTop: '10%',
        fontSize: 40
    },
    messageSend: {
        elevation: 5,
        backgroundColor: "#ddd",
        borderRadius: 10,
        minWidth: 40,
        padding: 10,
        margin: 20,
        marginLeft: 100,
        alignSelf: "flex-end",
    },
    messageReceived: {
        elevation: 5,
        backgroundColor: "#ccc",
        borderRadius: 10,
        padding: 10,
        margin: 20,
        marginRight: 100,
        alignSelf: "flex-start",
    },
    inputBox: {
        alignContent: 'flex-end',
        justifyContent: 'flex-end'
    }




});

export default styles;
