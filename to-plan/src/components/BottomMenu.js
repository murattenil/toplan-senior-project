import React, { useEffect, useRef, useState } from 'react';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { Ionicons, Feather } from '@expo/vector-icons';
import { createStackNavigator } from '@react-navigation/stack';



import Home from '../screens/HomeScreen/Home';
import CreateAct from '../screens/CreateScreen/CreateAct';
import FindAct from '../screens/FindScreen/FindAct';
import Join from '../screens/JoinScreen/Join'
import Friends from '../screens/FriendsScreen/Friends';
import Profile from '../screens/ProfileScreen/Profile';
import Settings from '../screens/SettingsScreen/Settings';
import Chatinside from '../components/ChatInside/Chatinside';
import Suggest from '../screens/SuggestScreen/Suggest';

const Tab = createMaterialBottomTabNavigator();
const Stack = createStackNavigator();

const optionHeandler = () => ({
  headerShown: false,
});


function stackAct() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Home"
        component={Home}
        options={optionHeandler}
      />
      <Stack.Screen
        name="CreateAct"
        component={CreateAct}
        options={optionHeandler}
      />
      <Stack.Screen
        name="FindAct"
        component={FindAct}
        options={optionHeandler}
      />
      <Stack.Screen
        name="Join"
        component={Join}
        options={optionHeandler}
      />
      <Stack.Screen
        name="Suggest"
        component={Suggest}
        options={optionHeandler}
      />
    </Stack.Navigator>
  );
}

function stackFriends(socket) {
  console.log(socket)
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Friends"

        component={Friends}
        options={optionHeandler}
      />
      <Stack.Screen
        name="Chatinside"
        component={Chatinside}


        options={optionHeandler}
      />
    </Stack.Navigator>
  )
}

function stackProfile() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Profile"
        component={Profile}
        options={optionHeandler}
      />
      <Stack.Screen
        name="Settings"
        component={Settings}
        options={optionHeandler}
      />

    </Stack.Navigator>
  );
}

export default function BottomMenu() {

  /*  
 const [selected, setSelect] = useState(false)
 const animation = React.useRef(null)
 const isFirstRun = React.useRef(true)

 React.useEffect(() => {
     if (isFirstRun.current) {
         if (selected == true) { 
   animation.current.play(20, 20);
      setSelect == false
   } else {
     animation.current.play(20, 20)
     setSelect == true
   }
   isFirstRun.current == false;

 } else if (selected == true) {
   animation.current.play(20, 20);
 } else {
   animation.current.play(20, 20);
 }
})
 */
  return (
    <Tab.Navigator
      shifting="true"
      inactiveColor="black"
      activeColor="black"
      barStyle={{ backgroundColor: '#EAECDD' }}>
      <Tab.Screen
        options={{
          tabBarColor: '#EAECDD',
          tabBarIcon: ({ color }) => (
            /*    <Lottie
                 source={require('../../assets/lottie/home.json')}
                 autoPlay={false}
                 loop={false}
                 ref={animation}
               /> */
            <Ionicons name="ios-home" color={color} size={22} />
          ),
        }}
        name="Home"
        component={stackAct}

      />
      <Tab.Screen
        options={{
          tabBarColor: '#EAECDD',
          tabBarIcon: ({ color }) => (
            <Feather name="message-circle" color={color} size={22} />
          ),
        }}
        name="Friends"
        component={stackFriends}
      />

      <Tab.Screen
        options={{
          tabBarColor: '#EAECDD',
          tabBarIcon: ({ color }) => (
            <Ionicons name="md-person" color={color} size={22} />
          ),
        }}
        name="Profile"
        component={stackProfile}
      />
    </Tab.Navigator>

  );
}

