import React, { useState, useEffect } from 'react';
import { Ionicons } from '@expo/vector-icons';
import { View, Text, TouchableOpacity } from 'react-native';
import { styles } from './styles';
import { useDispatch, useSelector } from 'react-redux'
import { fetchuser } from '../../../redux/actions/userAction';

const Chatrooms = props => {

    const dispatch = useDispatch();

    const onPressJoin = (roomId) => {
        props.socket.emit("joinroom", roomId);
        props.navigation.navigate('Chatinside', {
            roomId: roomId,
            socket: props.socket,


        })
    }

    return (
        <TouchableOpacity

            onPress={() => onPressJoin(props.id)}
        >
            <View style={styles.container}>
                <View style={styles.chatContainer}>

                    <View style={styles.photoContainer}>
                        <Ionicons name="ios-camera" size={20} color="grey" />
                    </View>

                    <View style={styles.roomContainer}>
                        <Text style={styles.roomName}>
                            {props.name}
                        </Text>


                    </View>

                </View>
            </View>
        </TouchableOpacity>
    );
}


export default Chatrooms;
