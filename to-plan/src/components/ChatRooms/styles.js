import { StyleSheet, Dimensions } from 'react-native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export const styles = StyleSheet.create({
    container: {
        shadowColor: 'black',
        shadowOpacity: 0.25,
        shadowOffset: { width: 0, height: 2 },
        borderRadius: 10,
        shadowRadius: 8,
        backgroundColor: '#fff',
        elevation: 5,
        height: windowHeight / 9,
        width: windowWidth / 1.2,
        margin: '3%',
        flexDirection: 'column'
    },
    chatContainer: {
        marginTop: '2%',
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: '2%',
    },
    photoContainer: {
        height: windowHeight / 12,
        width: windowHeight / 12,
        borderWidth: 2,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    roomName: {
        marginLeft: '5%',
        fontSize: 20,
        color: 'black'
    },
    roomContainer:
    {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    }


})
