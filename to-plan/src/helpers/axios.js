import axios from "axios";
import { store } from "../../redux/store";
import AsyncStorage from '@react-native-community/async-storage';

const token = AsyncStorage.getItem('token')

const axiosInstance = axios.create({
    headers: {
        Authorization: token ? `Bearer ${token}` : ""
    }
})

axiosInstance.interceptors.request.use((req) => {
    const { auth } = store.getState();
    if (auth.token) {
        req.headers.Authorization = `Bearer ${auth.token}`
    }
    return req;
})

export default axiosInstance;