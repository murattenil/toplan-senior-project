import { StyleSheet, Dimensions } from 'react-native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: '#EAECDD',
    alignItems: 'center'
  },
  name: {
    marginTop: '15%',
    fontSize: 40,
    color: 'black'
  },
  findButton: {
    marginTop: '35%',
    marginRight: '30%',
    width: windowHeight / 5,
    height: windowHeight / 5,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: .5,
    borderColor: 'black',
    backgroundColor: '#fff',
    borderRadius: 20,
  },
  createButton: {
    marginTop: '-7%',
    marginLeft: '30%',
    width: windowHeight / 5,
    height: windowHeight / 5,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: .5,
    borderColor: 'black',
    backgroundColor: '#fff',
    borderRadius: 20,
  },
  suggestButton: {
    marginTop: '15%',
    width: windowWidth / 2,
    height: windowHeight / 15,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    borderRadius: 20,
  },
  text: {
    color: 'black',
    fontSize: 20,

  }
});

export default styles;
