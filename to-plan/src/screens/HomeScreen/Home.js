import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import styles from './styles'
import Create from '../CreateScreen/CreateAct';
import Find from '../FindScreen/FindAct';
import Suggest from '../SuggestScreen/Suggest';

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.name}> TO-PLAN </Text>

        <TouchableOpacity
          style={styles.findButton}
          onPress={() => this.props.navigation.navigate(Find)}
        >
          <Text style={styles.text}>Find Activity</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.createButton}
          onPress={() => this.props.navigation.navigate(Create)}
        >
          <Text style={styles.text}>Create Activity</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.suggestButton}
          onPress={() => this.props.navigation.navigate(Suggest)}
        >
          <Text style={styles.text}>If You Need a Suggest</Text>
        </TouchableOpacity>

      </View>
    );
  }
}
