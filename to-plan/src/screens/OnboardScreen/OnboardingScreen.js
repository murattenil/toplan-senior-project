import React, { Component } from 'react';
import { StatusBar } from 'expo-status-bar';
import { View, Text, StyleSheet, FlatList, Animated, Image, Dimensions, TouchableOpacity } from 'react-native';


const { width, height } = Dimensions.get('screen');
const bgs = ['#A5BBFF', '#DDBEFE', '#FF63ED', '#B98EFF'];
const DATA = [
  {
    key: '3571572',
    title: 'TO-PLAN',
    description: ' To-Plan provide to find your activity pal ',
    image: 'https://img-premium.flaticon.com/png/512/2057/2057748.png?token=exp=1623578579~hmac=575a94b73237d0befb8f4b8e7deeaf66'
  },
  {
    key: '3571747',
    title: 'Create Activity',
    description: 'You can create activity and find activity friend.',
    image: 'https://image.flaticon.com/icons/png/512/1420/1420868.png'
  },
  {
    key: '3571680',
    title: 'Join Activity ',
    description: 'You can search and join all kinds of activities near you',
    image: 'https://img-premium.flaticon.com/png/512/993/993723.png?token=exp=1623579426~hmac=6d00acacaaf3094c4c5428d5ff61cdd8'
  },
  {
    key: '3571603',
    title: 'Have a good time!',
    description: 'Meet new people and enjoy',
    image: 'https://image.flaticon.com/icons/png/512/2058/2058666.png'
  }
]

const Indicator = ({ scrollX }) => {
  return <View style={{ position: 'absolute', bottom: 100, flexDirection: 'row' }}>
    {DATA.map((_, i) => {
      const inputRange = [(i - 1) * width, i * width, (i + 1) * width];

      const scale = scrollX.interpolate({
        inputRange,
        outputRange: [0.8, 1.4, 0.8],
        extrapolate: 'clamp',
      });
      const opacity = scrollX.interpolate({
        inputRange,
        outputRange: [0.6, 0.9, 0.6],
        extrapolate: 'clamp',
      })
      return <Animated.View
        key={'indicator-${i}'}
        style={{
          height: 10,
          width: 10,
          borderRadius: 5,
          backgroundColor: '#fff',
          opacity,
          margin: 10,
          transform: [
            {
              scale,
            }
          ]
        }}
      />
    })}
  </View>
};

const Backdrop = ({ scrollX }) => {
  const backgroundColor = scrollX.interpolate({
    inputRange: bgs.map((_, i) => i * width),
    outputRange: bgs.map((bg) => bg),
  })
  return (
    <Animated.View
      style={[StyleSheet.absoluteFillObject,
      {
        backgroundColor,
      },
      ]}
    />
  )
}

const Square = ({ scrollX }) => {
  const YOLO = Animated.modulo(Animated.divide(
    Animated.modulo(scrollX, width), new Animated.Value(width)),
    1
  );

  const rotate = YOLO.interpolate({
    inputRange: [0, 0.5, 1],
    outputRange: ['35deg', '0deg', '35deg']
  })

  const translateX = YOLO.interpolate({
    inputRange: [0, 0.5, 1],
    outputRange: [0, -height, 0]
  })

  return <Animated.View
    style={{
      width: height,
      height: height,
      backgroundColor: '#fff',
      borderRadius: 75,
      position: 'absolute',
      top: -height * 0.6,
      transform: [
        {
          rotate
        },
        {
          translateX
        }
      ]
    }}
  />
}

const Onboard = props => {
  const scrollX = React.useRef(new Animated.Value(0)).current;

  return (
    <View style={styles.container}>
      <Backdrop scrollX={scrollX} />
      <Square scrollX={scrollX} />
      <Animated.FlatList
        data={DATA}
        keyExtractor={item => item.key}
        horizontal
        pagingEnabled
        scrollEventThrottle={32}
        onScroll={Animated.event(
          [{ nativeEvent: { contentOffset: { x: scrollX } } }],
          { useNativeDriver: false }
        )}
        contentContainerStyle={{ paddingBottom: '10%' }}
        showsHorizontalScrollIndicator={false}
        renderItem={({ item }) => {
          return (
            <View style={{ width, alignItems: 'center', padding: '3%' }}>
              <View style={{ flex: .7, justifyContent: 'center' }}>
                <Image
                  style={{ width: width / 2, height: width / 2, resizeMode: 'contain' }}
                  source={{ uri: item.image }} />
              </View>
              <View style={{ flex: .3 }}>
                <Text style={{
                  color: 'black',
                  fontWeight: '800',
                  fontSize: 42,
                  marginBottom: '3%'
                }}>{item.title}</Text>
                <Text style={{
                  color: 'black',
                  fontWeight: '300',
                  fontSize: 24,
                }}>{item.description}</Text>

              </View>
              <TouchableOpacity
                style={{
                  backgroundColor: '#fff',
                  justifyContent: 'center',
                  marginLeft: '70%',
                  borderRadius: 10,
                  alignItems: 'center',
                  height: 40,
                  width: 40
                }}
                onPress={() => props.navigation.navigate('Login')}
              >
                <Text>Skip</Text>
              </TouchableOpacity>
            </View>


          );
        }}
      />
      <Indicator scrollX={scrollX} />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  }

})

export default Onboard;