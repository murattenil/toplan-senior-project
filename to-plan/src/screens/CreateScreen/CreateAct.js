import React, { Component, useState, useEffect } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Alert,
  Modal,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard,
  ScrollView,
  Platform
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import styles from './styles';
import { useDispatch, useSelector } from 'react-redux';
import { createpost, fetchposts } from '../../../redux/actions/postAction'
import { getCategories } from '../../../redux/actions/categoryAction'
import { getLocations } from '../../../redux/actions/locationAction';
import { Picker } from '@react-native-picker/picker';

const CreateAct = props => {
  const dispatch = useDispatch();
  const createPost = useSelector(state => state.createPost);
  const categoryReducer = useSelector(state => state.categoryReducer);
  const locationReducer = useSelector(state => state.locationReducer);
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [category, setCategory] = useState("");
  const [time, setTime] = useState("");
  const [location, setLocation] = useState("");
  const [isModalVisible, setisModelVisible] = useState(false)


  useEffect(() => {

    dispatch(getCategories())
  }, [])

  useEffect(() => {

    dispatch(getLocations())
  }, [])

  const onSubmit = () => {
    const data = {
      title, category, description, time, location
    }
    dispatch(createpost(data, props.navigation))
      .then(() => {
        if (title || description || time != '') {
          alert('Activity Successfully Created');
        }

      }).catch(() => {
        alert('Please fill in blanks', [{ text: 'OK' }])
      })
  }
  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.backArrow}
        onPress={() => props.navigation.goBack()}
      >
        <Ionicons name="ios-arrow-back" size={30} color="black" />
      </TouchableOpacity>

      <Text style={styles.title}>TO-PLAN</Text>
      <Text style={styles.secTitle}>Create Activity</Text>
      <KeyboardAvoidingView
        behavior={Platform.OS === "ios" ? "padding" : null}
        style={{ flex: 1 }}
      >
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <ScrollView showsHorizontalScrollIndicator={false}>
            <View style={styles.categories}>
              <Text style={styles.categoryText}>Choose Category: </Text>
              <View style={styles.pickerBox}>
                <Picker style={styles.picker} selectedValue={category} onValueChange={(itemValue, itemIndex) => setCategory(itemValue)}>
                  {
                    categoryReducer.categories && categoryReducer.categories.map(item =>
                      <Picker.Item label={item.name} value={item._id} key={item._id}></Picker.Item>
                    )
                  }
                </Picker>
              </View>

            </View >

            <View style={styles.categories}>

              <Text style={styles.categoryText}>Choose Location: </Text>
              <View style={styles.pickerBox}>
                <Picker style={styles.picker} selectedValue={location} onValueChange={(itemValue, itemIndex) => setLocation(itemValue)}>
                  {
                    locationReducer.locations && locationReducer.locations.map(item =>
                      <Picker.Item label={item.name} value={item._id} key={item._id}></Picker.Item>
                    )
                  }
                </Picker>
              </View>
            </View >


            <TextInput style={styles.locConteiner}
              placeholder="Enter Title"
              paddingStart='3%'
              onChangeText={(e) => setTitle(e)}
            />

            <TextInput style={styles.infoConteiner}
              placeholder="Enter Description"
              paddingStart='3%'
              onChangeText={(e) => setDescription(e)}
            />

            <TextInput style={styles.timeConteiner}
              placeholder="Time"
              paddingStart='3%'
              onChangeText={(e) => setTime(e)}
            />

            <TouchableOpacity style={styles.createButton}
              onPress={() => onSubmit()}
            >
              <Text style={styles.categoryText}>Create</Text>
            </TouchableOpacity>
          </ScrollView>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    </View >
  );
}

export default CreateAct;
