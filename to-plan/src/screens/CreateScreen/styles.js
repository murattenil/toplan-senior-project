import { StyleSheet, Dimensions } from 'react-native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#EAECDD',
        alignItems: "center",
    },
    backArrow: {
        position: 'absolute',
        top: '5%',
        left: '3%'

    },
    pickerBox: {
        marginBottom: '8%',
        height: windowHeight / 16,
        width: windowWidth / 3,
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white'
    },
    categories: {
        marginTop: '6%',
        marginBottom: '-8%',
        justifyContent: 'center',
        flexDirection: 'row',
        alignSelf: 'center'
    },
    categoryText: {
        fontSize: 20,
        paddingStart: '3%',
        color: 'black'
    },
    picker: {
        marginTop: '-11%',
        height: windowHeight / 7,
        width: windowHeight / 7,
    },
    title: {
        marginTop: '10%',
        fontSize: 40
    },
    secTitle: {
        marginLeft: '35%',
        fontSize: 16,
        color: 'grey'
    },
    categoryBox: {
        backgroundColor: '#fff',
        height: windowHeight / 12,
        width: windowWidth / 3,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 25,
    },


    locConteiner: {
        backgroundColor: '#fff',
        marginTop: '3%',
        justifyContent: 'center',
        height: windowHeight / 17,
        width: windowWidth / 1.2,
        borderRadius: 25,
    },
    infoConteiner: {
        marginTop: '3%',
        backgroundColor: '#fff',
        height: windowHeight / 7,
        width: windowWidth / 1.2,
        borderRadius: 25,
    },
    timeConteiner: {
        marginTop: '3%',
        backgroundColor: '#fff',
        height: windowHeight / 17,
        width: windowWidth / 4,
        borderRadius: 15,
        alignSelf: 'center'
    },
    createButton: {
        marginTop: '5%',
        justifyContent: 'center',
        alignItems: 'center',
        height: windowHeight / 16,
        width: windowWidth / 1.4,
        backgroundColor: '#fff',
        borderRadius: 25,
        alignSelf: 'center'
    },





});
export default styles;