import { StyleSheet, Dimensions } from 'react-native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: '#EAECDD',
    alignItems: 'center'
  },
  backArrow: {
    position: 'absolute',
    top: '5%',
    left: '3%'

  },
  pickerBox: {
    marginLeft: '2%',
    marginBottom: '8%',
    height: windowHeight / 18,
    width: windowWidth / 3,
    borderRadius: 20,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center'
  },
  categories: {
    marginTop: '5%',
    marginBottom: '-10%',
    justifyContent: 'center',
    flexDirection: 'row',
    alignSelf: 'center'
  },
  categoryText: {
    fontSize: 20,
    paddingStart: '3%',
    color: 'black'
  },
  picker: {
    marginTop: '-10%',
    height: windowHeight / 7,
    width: windowHeight / 7,
  },
  title: {
    marginTop: '10%',
    fontSize: 40
  },
  secTitle: {
    marginLeft: '35%',
    fontSize: 16,
    color: 'grey'
  },
  search: {
    backgroundColor: '#fff',
    marginTop: '4%',
    paddingHorizontal: '3%',
    justifyContent: 'center',
    height: windowHeight / 17,
    width: windowWidth / 1.2,
    borderRadius: 25,
  },

});

export default styles;
