import React, { Component, useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, TextInput, FlatList } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { useDispatch, useSelector } from 'react-redux'
import Cards from '../../components/Cards/Cards';
import styles from './styles'
import { Picker } from '@react-native-picker/picker';
import { fetchposts } from '../../../redux/actions/postAction';
import { getCategories } from '../../../redux/actions/categoryAction'


const FindAct = props => {

  const dispatch = useDispatch();
  const categoryReducer = useSelector(state => state.categoryReducer);
  const { activities } = useSelector(state => state.posts)
  const [data, setData] = useState(activities)
  const [category, setCategory] = useState("");


  useEffect(() => {
    dispatch(fetchposts());
  }, [dispatch]);


  useEffect(() => {

    dispatch(getCategories())
  }, [])


  searchActivities = (searchText) => {
    const text = searchText.toLowerCase();
    const filteredName = activities.filter((item) => {
      const itemData = `${item.title.toLowerCase(), item.category.name.toLowerCase()}`;

      return itemData.indexOf(text) > -1;

    });
    setData(filteredName);
  }



  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.backArrow}
        onPress={() => props.navigation.goBack()}
      >
        <Ionicons name="ios-arrow-back" size={30} color="black" />
      </TouchableOpacity>
      <Text style={styles.title}>TO-PLAN</Text>
      <Text style={styles.secTitle}>Find Activity</Text>
      <View style={styles.categories}>
        <Text style={styles.categoryText}>Choose Category: </Text>
        <View style={styles.pickerBox}>
          <Picker style={styles.picker} selectedValue={category} onValueChange={(itemValue, itemIndex) => setCategory(itemValue)}>
            {
              categoryReducer.categories && categoryReducer.categories.map(item =>
                <Picker.Item label={item.name} value={item._id} key={item._id}></Picker.Item>
              )
            }
          </Picker>
        </View>

      </View>

      <FlatList
        style={{ marginTop: '2%' }}
        data={data}
        keyExtractor={item => item._id}
        renderItem={({ item }) => (
          <Cards
            navigation={props.navigation}
            title={item.title}
            category={item.category}
            description={item.description}
            time={item.time}
            location={item.location}
            id={item._id}
            name={item.name}
          />
        )}
        ListHeaderComponent={
          <View style={styles.container}>
            <TextInput
              style={styles.search}
              placeholder='Search Activity'
              onChangeText={(text) => searchActivities(text)}
            />
          </View>

        }
      />

    </View>
  );

}

export default FindAct;
