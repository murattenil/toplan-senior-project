import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Modal } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import AsyncStorage from '@react-native-community/async-storage';
import styles from '../SettingsScreen/styles';



export default class Settings extends Component {
    onPressLogout = async () => {
        await AsyncStorage.removeItem('token');
        this.props.navigation.replace("Login")

    }

    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity style={styles.backArrow}
                    onPress={() => this.props.navigation.goBack()}
                >
                    <Ionicons name="ios-arrow-back" size={30} color="black" />
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => this.onPressLogout()}
                    style={styles.logoutButton}
                >
                    <Text style={styles.buttonText}>Logout</Text>
                </TouchableOpacity>
            </View>
        );
    }

}



