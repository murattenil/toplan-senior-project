import { StyleSheet, Dimensions } from 'react-native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({

    container: {
        flex: 1,
        width: windowWidth,
        height: windowHeight,
        backgroundColor: '#EAECDD',
        alignItems: 'center'
    },
    backArrow: {
        position: 'absolute',
        top: '5%',
        left: '3%'
    },
    logoutButton: {
        marginTop: '30%',
        height: windowHeight / 15,
        width: windowWidth / 5,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'red',
        alignSelf: 'center',
        borderRadius: 10,
    },
    buttonText: {
        fontSize: 16,
        color: 'white'
    }
})


export default styles;