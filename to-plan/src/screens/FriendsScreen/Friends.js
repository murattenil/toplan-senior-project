import React, { useEffect, useState } from 'react';
import { View, Text, FlatList, TouchableOpacity } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import io from 'socket.io-client';
import { Ionicons } from '@expo/vector-icons';
import AsyncStorage from '@react-native-community/async-storage';
import styles from './styles';
import Chatrooms from '../../components/ChatRooms/Chatrooms'
//@react-native-picker/picker
import { fetchchats, afterMessage, getAllRooms } from '../../../redux/actions/chatAction';


const Friends = props => {
  const dispatch = useDispatch();
  //const [message, setMessage] = useState('');
  const allRooms = useSelector((state) => state.allRooms);


  const [socket, setSocket] = useState(null);

  const InitializeSocket = async () => {

    const newSocket = io('http://192.168.1.35:5000');

    newSocket.on('connect', () => {
      console.log("SOCKET")
      setSocket(newSocket)
    })
    newSocket.on('disconnect', () => {
      setSocket('xd')
    })
    console.log("SOCKET IN FRIENDS" + newSocket.id)
    await setSocket(newSocket)

  }

  useEffect(() => {
    InitializeSocket();
    dispatch(getAllRooms());


  }, [dispatch])



  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.backArrow}
        onPress={() => props.navigation.goBack()}
      >
        <Ionicons name="ios-arrow-back" size={30} color="black" />
      </TouchableOpacity>
      <Text style={styles.title}>TO-PLAN</Text>
      <Text style={styles.secTitle}>Friends</Text>

      <FlatList
        style={{ marginTop: '1%' }}
        data={allRooms.rooms}
        keyExtractor={item => item._id}
        renderItem={({ item }) => (
          <Chatrooms
            navigation={props.navigation}
            name={item.name}
            //time={item.time} 
            id={item._id}
            socket={socket}
          />
        )}
      />

    </View>
  );
}

export default Friends