import React, { Component, useEffect } from 'react';
import { View, Text, TouchableOpacity, SafeAreaView, TextInput } from 'react-native';
import styles from './styles'
import { Ionicons, Feather } from '@expo/vector-icons';
import Animated from 'react-native-reanimated';
import BottomSheet from 'reanimated-bottom-sheet';
import AsyncStorage from '@react-native-community/async-storage';
import { useDispatch, useSelector } from 'react-redux'
import { IsLoggedIn } from '../../../redux/actions/isLoggedIn'
import { authAction } from '../../../redux/actions/authAction';

const Profile = props => {

  /*   const dispatch = useDispatch();
    const authReducer = useSelector(state => state.authReducer);
  
  
    useEffect(() => {
      dispatch(IsLoggedIn());
    }, [dispatch]);
  
    useEffect(() => {
      dispatch(authAction());
    }, []);
   */

  const renderInner = () => (
    <View style={styles.panel}>
      <View style={{ alignItems: 'center' }}>
        <Text style={styles.panelTitle}>Upload Photo</Text>
        <Text style={styles.panelSubtitle}>Choose Your Profile Picture</Text>
      </View>

      <TouchableOpacity style={styles.panelButton}>
        <Text style={styles.panelButtonTitle}>Take Photo</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.panelButton}>
        <Text style={styles.panelButtonTitle}>Choose From Library</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => this.bs.current.snapTo(1)}
        style={styles.panelButton}>
        <Text style={styles.panelButtonTitle}>Cancel</Text>
      </TouchableOpacity>

    </View>
  );

  const renderHeader = () => (
    <View style={styles.header}>
      <View style={styles.panelHeader}>
        <View style={styles.panelHandle}></View>
      </View>
    </View>
  );
  const bs = React.useRef();
  const fall = new Animated.Value(1);

  return (
    <SafeAreaView style={styles.container}>
      <BottomSheet
        ref={bs}
        snapPoints={[330, 0]}
        renderContent={renderInner}
        renderHeader={renderHeader}
        initialSnap={1}
        callbackNode={fall}
        enabledGestureInteraction={true}
      />
      <TouchableOpacity style={styles.editProfile}
        onPress={() => props.navigation.navigate('Settings')}
      >
        <Ionicons name="ios-cog" size={24} color="black" />
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.profilePicture}
        onPress={() => bs.current.snapTo(0)}
      >
        <Ionicons name="ios-camera" size={24} color="black" />
      </TouchableOpacity>
      <Text style={styles.name} >
        {/*   {
          authReducer.user.map(item => item.name)
        } */}
        x
      </Text>

      <View style={styles.containerActivities}>
        <Text style={styles.sportartText}>Enter Your Age</Text>
        <TextInput
          style={styles.sportartInput}
          placeholderTextColor="black"
          paddingStart='3%'
        > </TextInput>
        <Text style={styles.sportartText}>Enter Your Gender</Text>
        <TextInput
          style={styles.sportartInput}
          placeholderTextColor="black"
          paddingStart='3%'
        > </TextInput>
        <View style={styles.infoContainer}>
          <Text style={styles.sportartText}>Your Rating: </Text>
          <Text style={styles.sportartText}> 4.4 </Text>
        </View>

        <View style={styles.infoContainer}>
          <Text style={styles.sportartText}>Your Sport Activity History: </Text>
          <Text style={styles.sportartText}> 45 </Text>
        </View>

        <View style={styles.infoContainer}>
          <Text style={styles.sportartText}>Your Art Activity History: </Text>
          <Text style={styles.sportartText}> 12 </Text>
        </View>

        <TouchableOpacity
          style={styles.saveButton}
        >
          <Text style={styles.sportartText}>Save</Text>
        </TouchableOpacity>

      </View>


    </SafeAreaView >
  );
}

export default Profile;