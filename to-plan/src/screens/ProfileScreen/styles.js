import { StyleSheet, Dimensions } from 'react-native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#EAECDD',
    alignItems: 'center',

  },
  editProfile: {
    marginTop: '7%',
    marginRight: '3%',
    position: 'absolute',
    right: 0,
    top: 0
  },
  profilePicture: {
    marginTop: '5%',
    width: windowHeight / 6,
    height: windowHeight / 6,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#EAECDD',
    borderRadius: 20,
    borderWidth: 1,
    borderColor: 'black',
  },
  name: {
    marginTop: '2%',
    marginBottom: '2%',
    fontSize: 30,
    color: 'black'
  },
  containerActivities: {
    flex: 1,
    flexDirection: 'column',

    marginTop: '2%'
  },
  sportartText: {
    fontSize: 22,
    color: 'black'
  },
  sportartInput: {
    marginTop: '3%',
    height: windowHeight / 17,
    width: windowWidth / 1.2,
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 25,
  },
  saveButton: {
    marginTop: '5%',
    height: windowHeight / 15,
    width: windowWidth / 5,
    justifyContent: 'center',
    alignItems: 'center',
    color: '#EAECDD',
    borderWidth: 1,
    borderColor: 'black',
    alignSelf: 'center',
    borderRadius: 20,
  },
  infoContainer: {
    marginTop: '5%',
    flexDirection: 'row',
    alignItems: 'center'
  },

  //panel styles
  header: {
    backgroundColor: '#EAECDD',
    shadowColor: '#333333',
    shadowOffset: { width: -1, height: -3 },
    shadowRadius: 2,
    shadowOpacity: 0.4,
    paddingTop: '3%',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20

  },
  panelHeader: {
    alignItems: 'center'
  },
  panelHandle: {
    height: 8,
    width: 40,
    borderRadius: 4,
    backgroundColor: 'black',
    marginBottom: '7%'
  },
  panel: {
    backgroundColor: '#EAECDD',
    alignItems: 'center',
    height: '100%'
  },
  panelTitle: {
    fontSize: 24,
    fontWeight: 'bold'
  },
  panelSubtitle: {
    fontSize: 14,
    color: 'black'
  },
  panelButton: {
    height: windowHeight / 15,
    width: windowWidth / 1.1,
    backgroundColor: '#EAECDD',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
    borderWidth: 1,
    borderColor: 'black',
    marginTop: '3%'
  },
  panelButtonTitle: {
    color: 'black',
    fontSize: 16,
  }


});
export default styles;