import React, { Component, useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, TextInput, FlatList } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { Picker } from '@react-native-picker/picker';
import styles from './styles'
import { useDispatch, useSelector } from 'react-redux';
import { getCategories } from '../../../redux/actions/categoryAction'
import { getLocations } from '../../../redux/actions/locationAction';

const Suggest = props => {
    const dispatch = useDispatch();
    const categoryReducer = useSelector(state => state.categoryReducer);
    const locationReducer = useSelector(state => state.locationReducer);
    const [category, setCategory] = useState("");
    const [location, setLocation] = useState("");

    useEffect(() => {
        dispatch(getCategories())
    }, [])

    useEffect(() => {
        dispatch(getLocations())
    }, [])

    return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.backArrow}
                onPress={() => props.navigation.goBack()}
            >
                <Ionicons name="ios-arrow-back" size={30} color="black" />
            </TouchableOpacity>
            <Text style={styles.title}>TO-PLAN</Text>
            <Text style={styles.secTitle}>Suggestion</Text>

            <View style={styles.inputView}>
                <Text style={styles.buttonText}>Choose Your District</Text>
                <View style={styles.pickerBox}>
                    <Picker style={styles.picker} selectedValue={location} onValueChange={(itemValue, itemIndex) => setLocation(itemValue)}>
                        {
                            locationReducer.locations && locationReducer.locations.map(item =>
                                <Picker.Item label={item.name} value={item._id} key={item._id}></Picker.Item>
                            )
                        }
                    </Picker>
                </View>

                <View style={styles.pickerBox}>
                    <Text style={styles.buttonText}>Choose Your Category</Text>
                    <Picker style={styles.picker} selectedValue={category} onValueChange={(itemValue, itemIndex) => setCategory(itemValue)}>
                        {
                            categoryReducer.categories && categoryReducer.categories.map(item =>
                                <Picker.Item label={item.name} value={item._id} key={item._id}></Picker.Item>
                            )
                        }
                    </Picker>
                </View>

            </View>

            <TouchableOpacity
                style={styles.button}
            >
                <Text style={styles.buttonText}> Suggest me</Text>
            </TouchableOpacity>

        </View>
    );

}

export default Suggest;
