import { StyleSheet, Dimensions } from 'react-native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: '#EAECDD',
        alignItems: 'center'
    },
    backArrow: {
        position: 'absolute',
        top: '5%',
        left: '3%'

    },
    pickerBox: {
        marginTop: '4%',
        marginBottom: '5%',
        height: windowHeight / 16,
        width: windowWidth / 3,
        borderRadius: 20,

        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white'
    },
    title: {
        marginTop: '10%',
        fontSize: 40,
    },
    picker: {
        marginTop: '-11%',
        height: windowHeight / 7,
        width: windowHeight / 7,
    },
    secTitle: {
        marginLeft: '35%',
        fontSize: 16,
        color: 'grey'
    },
    input: {
        marginTop: '10%',
        height: windowHeight / 17,
        width: windowWidth / 1.2,
        borderWidth: .5,
        borderColor: 'black',
        borderRadius: 25,
        padding: 10
    },
    button: {
        marginTop: '5%',
        justifyContent: 'center',
        alignItems: 'center',
        height: windowHeight / 16,
        width: windowWidth / 2.5,
        backgroundColor: '#fff',
        borderRadius: 25,
        alignSelf: 'center'
    },
    buttonText: {
        fontSize: 20
    },
    inputView: {
        marginTop: '10%',

    }


});

export default styles;
