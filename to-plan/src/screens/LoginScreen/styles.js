import { StyleSheet, Dimensions } from 'react-native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: '#EAECDD',
    alignItems: 'center',

  },
  secTitle: {
    marginRight: '35%',
    fontSize: 20,
    color: 'grey',
    marginTop: '15%'
  },
  name: {
    fontSize: 42,
    color: 'black',
    marginBottom: '10%'
  },
  emailText: {
    color: 'black',
    fontSize: 18,
    marginTop: '5%'
  },
  inputEmail: {
    color: 'grey',
    fontSize: 20,
    paddingStart: '3%',
    marginTop: '2%',
    borderRadius: 15,
    backgroundColor: 'white',
    width: windowWidth / 1.2,
    height: windowHeight / 15,
  },
  button: {
    width: windowWidth / 3,
    height: windowHeight / 15,
    backgroundColor: 'white',
    justifyContent: "center",
    alignItems: "center",
    marginTop: "5%",
    borderRadius: 18,
    alignSelf: 'center'

  },
  buttonText: {
    color: 'black',
    fontSize: 18
  },
  signButton: {
    alignSelf: 'flex-end',
    fontSize: 16,
    color: 'grey',
    marginTop: '3%'
  }

});

export default styles;
