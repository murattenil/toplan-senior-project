import React, { Component, useState, useEffect } from "react";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  Platform,
  ActivityIndicator,
  Alert,
  TouchableWithoutFeedback,
  Keyboard,
  ScrollView
} from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { registerUser, loginUser } from "../../../redux/actions/authAction";
import { IsLoggedIn } from "../../../redux/actions/isLoggedIn";
import { useDispatch, useSelector } from "react-redux";
import styles from "./styles";
import BottomMenu from "../../components/BottomMenu";
import authReducer from "../../../redux/reducers/authReducer";
import AsyncStorage from "@react-native-community/async-storage";
import Onboard from "../OnboardScreen/OnboardingScreen";

function LoginScreen({ navigation }) {
  const auth = useSelector((state) => state.auth);
  const [token, setToken] = useState(null);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const dispatch = useDispatch();

  const form = (

    <View style={styles.container}>
      <Text style={styles.secTitle}>Welcome!</Text>
      <Text style={styles.name}> TO-PLAN </Text>
      <ScrollView>
        <Text style={styles.emailText}> Enter your email </Text>
        <TextInput
          style={styles.inputEmail}
          placeholder="example@email.com"
          autoFocus

          onChangeText={(e) => setEmail(e)}
        />
        <Text style={styles.emailText}> Enter your password</Text>
        <TextInput
          style={styles.inputEmail}
          placeholder="********"
          secureTextEntry={true}
          autoFocus

          onChangeText={(e) => setPassword(e)}
        />
        <TouchableOpacity style={styles.button} onPress={() => onSubmit()}>
          <Text style={styles.buttonText}>Login</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => navigation.navigate("SignUp")}>
          <Text style={styles.signButton}>Sign Up</Text>
        </TouchableOpacity>
      </ScrollView>
    </View >
  );


  const onSubmit = () => {
    console.log("email-" + email)
    dispatch(loginUser(email, password, navigation));

  };

  return (

    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
      style={{ flex: 1 }}>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        {form}
        {/* <View>
        <Text>
          {auth.done === true ? "" : "Check your email or password"}
        </Text>
      </View> */}
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );


}

function SignupScreen({ navigation }) {
  const register = useSelector((state) => state.register);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");
  const dispatch = useDispatch();

  const onSubmit = () => {
    console.log(email, name)
    dispatch(registerUser(name, email, password, navigation))
      .then(() => {
        /*  if (email || name || password == '') {
           alert('Please fill in blanks', [{ text: 'OK' }])
         } else { */
        alert('Acount Created Successfully')

      }).catch(() => {
        alert('Error', [{ text: 'OK' }])
      })

  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
      style={{ flex: 1 }}
    >
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.container}>
          <Text style={styles.secTitle}>Welcome!</Text>
          <Text style={styles.name}> TO-PLAN </Text>
          <ScrollView>
            <Text style={styles.emailText}> Enter your name </Text>
            <TextInput
              style={styles.inputEmail}
              placeholder="name surname"
              autoFocus
              onChangeText={(e) => setName(e)}
            />
            <Text style={styles.emailText}> Enter your email</Text>
            <TextInput
              style={styles.inputEmail}
              placeholder="example@email.com"
              autoFocus
              onChangeText={(e) => setEmail(e)}
            />
            <Text style={styles.emailText}> Enter your password</Text>
            <TextInput
              style={styles.inputEmail}
              placeholder="********"
              secureTextEntry={true}
              autoFocus
              onChangeText={(e) => setPassword(e)}
            />
            <TouchableOpacity style={styles.button} onPress={() => onSubmit()}>
              <Text style={styles.buttonText}>Sign Up</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => navigation.goBack()}>
              <Text style={styles.signButton}>Go Login</Text>
            </TouchableOpacity>
          </ScrollView>
        </View>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );
}

function LoadingScreen({ navigation }) {
  const auth = useSelector(state => state.auth);

  const detectLogin = async () => {
    const token = await AsyncStorage.getItem('token')

    if (token != null) {
      navigation.replace("Menu")
    } else {
      navigation.replace("Login")
    }
  }

  useEffect(() => {
    detectLogin()
  }, []);


  return (
    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      <ActivityIndicator size="large" color="blue" />
    </View>
  )

}


const Stack = createStackNavigator();

export function Login() {
  const auth = useSelector(state => state.auth)
  const dispatch = useDispatch()
  useEffect(() => {
    if (auth.done === false) {
      dispatch(IsLoggedIn())
    }
  }, [auth.done])
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}
        initialRouteName="Onboard"
      >
        <Stack.Screen name="Onboard" component={Onboard} />
        <Stack.Screen name="Loading" component={LoadingScreen} />
        <Stack.Screen name="Login" component={LoginScreen} />
        <Stack.Screen name="SignUp" component={SignupScreen} />
        <Stack.Screen name="Menu" component={BottomMenu} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

