import { StyleSheet, Dimensions } from 'react-native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({

  container: {
    flex: 1,
    width: windowWidth,
    height: windowHeight,
    backgroundColor: '#EAECDD',
    alignItems: 'center'
  },
  title: {
    marginTop: '10%',
    fontSize: 40
  },
  categoryTitle: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  secTitle: {
    marginLeft: '35%',
    fontSize: 16,
    color: 'grey'
  },
  infoContainer: {
    marginTop: '5%',
    height: windowHeight / 3,
    width: windowWidth / 1.1,
    backgroundColor: '#fff',
    borderRadius: 20,
    flexDirection: 'column',
  },
  text: {
    fontSize: 14,
    color: 'grey'
  },
  titleText: {
    fontWeight: '600',
    fontSize: 24,
    marginBottom: '5%'
  },
  name: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: '5%',
    position: 'absolute',
    bottom: '3%'
  },
  button: {
    marginTop: '10%',
    height: windowHeight / 16,
    width: windowWidth / 4,
    backgroundColor: 'white',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center'
  },
  backArrow: {
    position: 'absolute',
    top: '5%',
    left: '3%'
  },
  textContainer: {
    marginLeft: '5%',
    marginTop: '2%'
  },
  customRatingBarStyle: {
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: '5%',
  },
  starImgStyle: {
    width: 50,
    height: 50,
    resizeMode: 'cover'
  },
  ratingContainer: {
    marginTop: '7%',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  ratingText: {
    fontSize: 22,
    color: 'black'
  },
  textStyle: {
    fontSize: 22,
    marginTop: '3%'
  },
  ratingButton: {
    height: windowHeight / 18,
    width: windowWidth / 5,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '4%',
    backgroundColor: 'white',
    borderRadius: 10,
  }

});

export default styles;