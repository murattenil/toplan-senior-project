import React, { Component, useState } from 'react';
import { View, Text, TouchableOpacity, ScrollView, Image, Alert } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import styles from '../JoinScreen/styles';
import { useSelector } from 'react-redux'

const Join = props => {

  const { postId } = props.route.params;
  const post = useSelector(state => state.posts.activities.find(posts => posts._id == postId))

  const [defaultRating, setDefaultRating] = useState(3)
  const [maxRating, setMaxRating] = useState([1, 2, 3, 4, 5])

  const starFilled = 'https://raw.githubusercontent.com/tranhonghan/images/main/star_filled.png'
  const starCorner = 'https://raw.githubusercontent.com/tranhonghan/images/main/star_corner.png'

  const CustomRatingBar = () => {
    return (
      <View style={styles.customRatingBarStyle}>
        {
          maxRating.map((item, key) => {
            return (
              <TouchableOpacity
                activeOpacity={0.7}
                key={item}
                onPress={() => setDefaultRating(item)}
              >
                <Image
                  style={styles.starImgStyle}
                  source={
                    item <= defaultRating ? { uri: starFilled } : { uri: starCorner }
                  }
                />
              </TouchableOpacity>
            )
          })
        }
      </View>
    )
  }



  return (

    <View style={styles.container}>
      <TouchableOpacity style={styles.backArrow}
        onPress={() => props.navigation.goBack()}
      >
        <Ionicons name="ios-arrow-back" size={30} color="black" />
      </TouchableOpacity>

      <Text style={styles.title}>TO-PLAN</Text>
      <Text style={styles.secTitle}>Join Activity</Text>
      <ScrollView >
        <View style={styles.infoContainer}>
          <View style={styles.textContainer}>
            <View style={styles.categoryTitle}>
              <Text style={styles.titleText}>{post.title}</Text>
              <Text style={styles.titleText}> - [ {post.category.name} ]</Text>
            </View>

            <Text style={styles.text}>{post.description}</Text>
          </View>

          <View style={styles.name}>
            <Text style={styles.text}>{post.time} - </Text>
            <Text style={styles.text}>{post.location} - </Text>
            <Text style={styles.text}>{post.name}</Text>
          </View>

        </View>



        <TouchableOpacity style={styles.button}
          onPress={() => props.navigation.navigate('Friends')}
        >
          <Text style={styles.text}>ENROLL</Text>
        </TouchableOpacity>

        <View style={styles.ratingContainer}>

          <Text style={styles.ratingText}>How was your Activity ?</Text>
          <CustomRatingBar />
          <Text style={styles.textStyle}>
            {defaultRating + ' / ' + maxRating.length}
          </Text>
          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.ratingButton}
            onPress={() => alert('Your rateing is ' + defaultRating + ' point. Thank you!')}
          >
            <Text style={styles.textStyle}> Rate</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );

}


export default Join;
